-- CreateTable
CREATE TABLE "File" (
    "id" TEXT NOT NULL,
    "user_id" INTEGER NOT NULL,
    "link" TEXT NOT NULL,
    "is_public" BOOLEAN NOT NULL DEFAULT true,
    "users_access" INTEGER[] DEFAULT ARRAY[]::INTEGER[],
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "File_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "File_id_link_key" ON "File"("id", "link");

-- AddForeignKey
ALTER TABLE "File" ADD CONSTRAINT "File_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
