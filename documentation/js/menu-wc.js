'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">user documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link" >AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-AuthModule-e8aac59282b9d7ab90a71c6cf933f09c9484e942b89e4c4fc19043abdeb5d32d62eb2eb32b59a212ae3869f1cae0cbb9c8a75f4d6ff51ea9df4b2d81dd4a7640"' : 'data-bs-target="#xs-controllers-links-module-AuthModule-e8aac59282b9d7ab90a71c6cf933f09c9484e942b89e4c4fc19043abdeb5d32d62eb2eb32b59a212ae3869f1cae0cbb9c8a75f4d6ff51ea9df4b2d81dd4a7640"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AuthModule-e8aac59282b9d7ab90a71c6cf933f09c9484e942b89e4c4fc19043abdeb5d32d62eb2eb32b59a212ae3869f1cae0cbb9c8a75f4d6ff51ea9df4b2d81dd4a7640"' :
                                            'id="xs-controllers-links-module-AuthModule-e8aac59282b9d7ab90a71c6cf933f09c9484e942b89e4c4fc19043abdeb5d32d62eb2eb32b59a212ae3869f1cae0cbb9c8a75f4d6ff51ea9df4b2d81dd4a7640"' }>
                                            <li class="link">
                                                <a href="controllers/AuthController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuthController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-AuthModule-e8aac59282b9d7ab90a71c6cf933f09c9484e942b89e4c4fc19043abdeb5d32d62eb2eb32b59a212ae3869f1cae0cbb9c8a75f4d6ff51ea9df4b2d81dd4a7640"' : 'data-bs-target="#xs-injectables-links-module-AuthModule-e8aac59282b9d7ab90a71c6cf933f09c9484e942b89e4c4fc19043abdeb5d32d62eb2eb32b59a212ae3869f1cae0cbb9c8a75f4d6ff51ea9df4b2d81dd4a7640"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-e8aac59282b9d7ab90a71c6cf933f09c9484e942b89e4c4fc19043abdeb5d32d62eb2eb32b59a212ae3869f1cae0cbb9c8a75f4d6ff51ea9df4b2d81dd4a7640"' :
                                        'id="xs-injectables-links-module-AuthModule-e8aac59282b9d7ab90a71c6cf933f09c9484e942b89e4c4fc19043abdeb5d32d62eb2eb32b59a212ae3869f1cae0cbb9c8a75f4d6ff51ea9df4b2d81dd4a7640"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuthService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/FileManagerModule.html" data-type="entity-link" >FileManagerModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-FileManagerModule-fa3762c446e50eed31a59c4948f054fa3bb01b8d1da73e79696c6c89cdfc77f63dfc5792cc1aae92c301ba113b47a5e8bc66078c679a7c72bfa4534beaafca29"' : 'data-bs-target="#xs-controllers-links-module-FileManagerModule-fa3762c446e50eed31a59c4948f054fa3bb01b8d1da73e79696c6c89cdfc77f63dfc5792cc1aae92c301ba113b47a5e8bc66078c679a7c72bfa4534beaafca29"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-FileManagerModule-fa3762c446e50eed31a59c4948f054fa3bb01b8d1da73e79696c6c89cdfc77f63dfc5792cc1aae92c301ba113b47a5e8bc66078c679a7c72bfa4534beaafca29"' :
                                            'id="xs-controllers-links-module-FileManagerModule-fa3762c446e50eed31a59c4948f054fa3bb01b8d1da73e79696c6c89cdfc77f63dfc5792cc1aae92c301ba113b47a5e8bc66078c679a7c72bfa4534beaafca29"' }>
                                            <li class="link">
                                                <a href="controllers/FileManagerController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FileManagerController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-FileManagerModule-fa3762c446e50eed31a59c4948f054fa3bb01b8d1da73e79696c6c89cdfc77f63dfc5792cc1aae92c301ba113b47a5e8bc66078c679a7c72bfa4534beaafca29"' : 'data-bs-target="#xs-injectables-links-module-FileManagerModule-fa3762c446e50eed31a59c4948f054fa3bb01b8d1da73e79696c6c89cdfc77f63dfc5792cc1aae92c301ba113b47a5e8bc66078c679a7c72bfa4534beaafca29"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-FileManagerModule-fa3762c446e50eed31a59c4948f054fa3bb01b8d1da73e79696c6c89cdfc77f63dfc5792cc1aae92c301ba113b47a5e8bc66078c679a7c72bfa4534beaafca29"' :
                                        'id="xs-injectables-links-module-FileManagerModule-fa3762c446e50eed31a59c4948f054fa3bb01b8d1da73e79696c6c89cdfc77f63dfc5792cc1aae92c301ba113b47a5e8bc66078c679a7c72bfa4534beaafca29"' }>
                                        <li class="link">
                                            <a href="injectables/FileManagerService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FileManagerService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/MessageModule.html" data-type="entity-link" >MessageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-MessageModule-080c0ab86b7c51eff6d7e100c3ea36d99e75a25f2a944d5d0923dabdab0edc513519e9d08d581473f99bafea06189ded5e2f721659c6391b4ea8b34b2fcf3e32"' : 'data-bs-target="#xs-controllers-links-module-MessageModule-080c0ab86b7c51eff6d7e100c3ea36d99e75a25f2a944d5d0923dabdab0edc513519e9d08d581473f99bafea06189ded5e2f721659c6391b4ea8b34b2fcf3e32"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-MessageModule-080c0ab86b7c51eff6d7e100c3ea36d99e75a25f2a944d5d0923dabdab0edc513519e9d08d581473f99bafea06189ded5e2f721659c6391b4ea8b34b2fcf3e32"' :
                                            'id="xs-controllers-links-module-MessageModule-080c0ab86b7c51eff6d7e100c3ea36d99e75a25f2a944d5d0923dabdab0edc513519e9d08d581473f99bafea06189ded5e2f721659c6391b4ea8b34b2fcf3e32"' }>
                                            <li class="link">
                                                <a href="controllers/MessageController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MessageController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-MessageModule-080c0ab86b7c51eff6d7e100c3ea36d99e75a25f2a944d5d0923dabdab0edc513519e9d08d581473f99bafea06189ded5e2f721659c6391b4ea8b34b2fcf3e32"' : 'data-bs-target="#xs-injectables-links-module-MessageModule-080c0ab86b7c51eff6d7e100c3ea36d99e75a25f2a944d5d0923dabdab0edc513519e9d08d581473f99bafea06189ded5e2f721659c6391b4ea8b34b2fcf3e32"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-MessageModule-080c0ab86b7c51eff6d7e100c3ea36d99e75a25f2a944d5d0923dabdab0edc513519e9d08d581473f99bafea06189ded5e2f721659c6391b4ea8b34b2fcf3e32"' :
                                        'id="xs-injectables-links-module-MessageModule-080c0ab86b7c51eff6d7e100c3ea36d99e75a25f2a944d5d0923dabdab0edc513519e9d08d581473f99bafea06189ded5e2f721659c6391b4ea8b34b2fcf3e32"' }>
                                        <li class="link">
                                            <a href="injectables/MessageService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MessageService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/OtpModule.html" data-type="entity-link" >OtpModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-OtpModule-e52466058449b45bb1457457544809fba41f2a7f940c2a62619ede5e2c29a1a58f620c8a1db5c037011b9d2225334a3dd704e6fdb4aa5929d0f4d94870c0b25b"' : 'data-bs-target="#xs-controllers-links-module-OtpModule-e52466058449b45bb1457457544809fba41f2a7f940c2a62619ede5e2c29a1a58f620c8a1db5c037011b9d2225334a3dd704e6fdb4aa5929d0f4d94870c0b25b"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-OtpModule-e52466058449b45bb1457457544809fba41f2a7f940c2a62619ede5e2c29a1a58f620c8a1db5c037011b9d2225334a3dd704e6fdb4aa5929d0f4d94870c0b25b"' :
                                            'id="xs-controllers-links-module-OtpModule-e52466058449b45bb1457457544809fba41f2a7f940c2a62619ede5e2c29a1a58f620c8a1db5c037011b9d2225334a3dd704e6fdb4aa5929d0f4d94870c0b25b"' }>
                                            <li class="link">
                                                <a href="controllers/OtpController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OtpController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-OtpModule-e52466058449b45bb1457457544809fba41f2a7f940c2a62619ede5e2c29a1a58f620c8a1db5c037011b9d2225334a3dd704e6fdb4aa5929d0f4d94870c0b25b"' : 'data-bs-target="#xs-injectables-links-module-OtpModule-e52466058449b45bb1457457544809fba41f2a7f940c2a62619ede5e2c29a1a58f620c8a1db5c037011b9d2225334a3dd704e6fdb4aa5929d0f4d94870c0b25b"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-OtpModule-e52466058449b45bb1457457544809fba41f2a7f940c2a62619ede5e2c29a1a58f620c8a1db5c037011b9d2225334a3dd704e6fdb4aa5929d0f4d94870c0b25b"' :
                                        'id="xs-injectables-links-module-OtpModule-e52466058449b45bb1457457544809fba41f2a7f940c2a62619ede5e2c29a1a58f620c8a1db5c037011b9d2225334a3dd704e6fdb4aa5929d0f4d94870c0b25b"' }>
                                        <li class="link">
                                            <a href="injectables/OtpService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OtpService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PrismaModule.html" data-type="entity-link" >PrismaModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-PrismaModule-d5544fa8655bfd79c139c840345379e7911c602b4b3d9444db84fa23e9e5a29f8908fdf7865391af9646b9804514452514bec13132fb300c3f5f7277fc297fe1"' : 'data-bs-target="#xs-injectables-links-module-PrismaModule-d5544fa8655bfd79c139c840345379e7911c602b4b3d9444db84fa23e9e5a29f8908fdf7865391af9646b9804514452514bec13132fb300c3f5f7277fc297fe1"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PrismaModule-d5544fa8655bfd79c139c840345379e7911c602b4b3d9444db84fa23e9e5a29f8908fdf7865391af9646b9804514452514bec13132fb300c3f5f7277fc297fe1"' :
                                        'id="xs-injectables-links-module-PrismaModule-d5544fa8655bfd79c139c840345379e7911c602b4b3d9444db84fa23e9e5a29f8908fdf7865391af9646b9804514452514bec13132fb300c3f5f7277fc297fe1"' }>
                                        <li class="link">
                                            <a href="injectables/PrismaService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PrismaService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ReferralCodeModule.html" data-type="entity-link" >ReferralCodeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-ReferralCodeModule-1b1083c85be76c6b1f6c57dba3f23075002c90fef8bb27e30bb010f036fc61db3eb4d8993456e42f2467e1751125edc82cb7e284df50fda77461291cb9dc8164"' : 'data-bs-target="#xs-controllers-links-module-ReferralCodeModule-1b1083c85be76c6b1f6c57dba3f23075002c90fef8bb27e30bb010f036fc61db3eb4d8993456e42f2467e1751125edc82cb7e284df50fda77461291cb9dc8164"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-ReferralCodeModule-1b1083c85be76c6b1f6c57dba3f23075002c90fef8bb27e30bb010f036fc61db3eb4d8993456e42f2467e1751125edc82cb7e284df50fda77461291cb9dc8164"' :
                                            'id="xs-controllers-links-module-ReferralCodeModule-1b1083c85be76c6b1f6c57dba3f23075002c90fef8bb27e30bb010f036fc61db3eb4d8993456e42f2467e1751125edc82cb7e284df50fda77461291cb9dc8164"' }>
                                            <li class="link">
                                                <a href="controllers/ReferralCodeController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ReferralCodeController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-ReferralCodeModule-1b1083c85be76c6b1f6c57dba3f23075002c90fef8bb27e30bb010f036fc61db3eb4d8993456e42f2467e1751125edc82cb7e284df50fda77461291cb9dc8164"' : 'data-bs-target="#xs-injectables-links-module-ReferralCodeModule-1b1083c85be76c6b1f6c57dba3f23075002c90fef8bb27e30bb010f036fc61db3eb4d8993456e42f2467e1751125edc82cb7e284df50fda77461291cb9dc8164"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ReferralCodeModule-1b1083c85be76c6b1f6c57dba3f23075002c90fef8bb27e30bb010f036fc61db3eb4d8993456e42f2467e1751125edc82cb7e284df50fda77461291cb9dc8164"' :
                                        'id="xs-injectables-links-module-ReferralCodeModule-1b1083c85be76c6b1f6c57dba3f23075002c90fef8bb27e30bb010f036fc61db3eb4d8993456e42f2467e1751125edc82cb7e284df50fda77461291cb9dc8164"' }>
                                        <li class="link">
                                            <a href="injectables/ReferralCodeRepository.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ReferralCodeRepository</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ReferralCodeService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ReferralCodeService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/RoleModule.html" data-type="entity-link" >RoleModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-RoleModule-092a929540306a0960ff0f8ecb51a60003ab7a542d87c9e28631a885ec048b820de89049b288a445ccf6bf2c39383834609929f4ddcdd196a77ac1f04162f719"' : 'data-bs-target="#xs-controllers-links-module-RoleModule-092a929540306a0960ff0f8ecb51a60003ab7a542d87c9e28631a885ec048b820de89049b288a445ccf6bf2c39383834609929f4ddcdd196a77ac1f04162f719"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-RoleModule-092a929540306a0960ff0f8ecb51a60003ab7a542d87c9e28631a885ec048b820de89049b288a445ccf6bf2c39383834609929f4ddcdd196a77ac1f04162f719"' :
                                            'id="xs-controllers-links-module-RoleModule-092a929540306a0960ff0f8ecb51a60003ab7a542d87c9e28631a885ec048b820de89049b288a445ccf6bf2c39383834609929f4ddcdd196a77ac1f04162f719"' }>
                                            <li class="link">
                                                <a href="controllers/RoleController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RoleController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-RoleModule-092a929540306a0960ff0f8ecb51a60003ab7a542d87c9e28631a885ec048b820de89049b288a445ccf6bf2c39383834609929f4ddcdd196a77ac1f04162f719"' : 'data-bs-target="#xs-injectables-links-module-RoleModule-092a929540306a0960ff0f8ecb51a60003ab7a542d87c9e28631a885ec048b820de89049b288a445ccf6bf2c39383834609929f4ddcdd196a77ac1f04162f719"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-RoleModule-092a929540306a0960ff0f8ecb51a60003ab7a542d87c9e28631a885ec048b820de89049b288a445ccf6bf2c39383834609929f4ddcdd196a77ac1f04162f719"' :
                                        'id="xs-injectables-links-module-RoleModule-092a929540306a0960ff0f8ecb51a60003ab7a542d87c9e28631a885ec048b820de89049b288a445ccf6bf2c39383834609929f4ddcdd196a77ac1f04162f719"' }>
                                        <li class="link">
                                            <a href="injectables/RoleService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RoleService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/SessionModule.html" data-type="entity-link" >SessionModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-SessionModule-8204f076b25a434f174491a14432bb1519f7a4b4016671d491a056ef97727c79ae7d715ecaca8c998c4fe20126a976ba7562666f7ee348fc6325ac8a0a7e389f"' : 'data-bs-target="#xs-injectables-links-module-SessionModule-8204f076b25a434f174491a14432bb1519f7a4b4016671d491a056ef97727c79ae7d715ecaca8c998c4fe20126a976ba7562666f7ee348fc6325ac8a0a7e389f"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-SessionModule-8204f076b25a434f174491a14432bb1519f7a4b4016671d491a056ef97727c79ae7d715ecaca8c998c4fe20126a976ba7562666f7ee348fc6325ac8a0a7e389f"' :
                                        'id="xs-injectables-links-module-SessionModule-8204f076b25a434f174491a14432bb1519f7a4b4016671d491a056ef97727c79ae7d715ecaca8c998c4fe20126a976ba7562666f7ee348fc6325ac8a0a7e389f"' }>
                                        <li class="link">
                                            <a href="injectables/SessionService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SessionService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-UserModule-1b1e0a24f74dd8d313757060077cb050275d06e096b7fe1aef351846cbb339f6e354db59cca90b72d7e52f5bd077196ed3a9418de5fadfa356db08c45b8539ea"' : 'data-bs-target="#xs-controllers-links-module-UserModule-1b1e0a24f74dd8d313757060077cb050275d06e096b7fe1aef351846cbb339f6e354db59cca90b72d7e52f5bd077196ed3a9418de5fadfa356db08c45b8539ea"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-1b1e0a24f74dd8d313757060077cb050275d06e096b7fe1aef351846cbb339f6e354db59cca90b72d7e52f5bd077196ed3a9418de5fadfa356db08c45b8539ea"' :
                                            'id="xs-controllers-links-module-UserModule-1b1e0a24f74dd8d313757060077cb050275d06e096b7fe1aef351846cbb339f6e354db59cca90b72d7e52f5bd077196ed3a9418de5fadfa356db08c45b8539ea"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-UserModule-1b1e0a24f74dd8d313757060077cb050275d06e096b7fe1aef351846cbb339f6e354db59cca90b72d7e52f5bd077196ed3a9418de5fadfa356db08c45b8539ea"' : 'data-bs-target="#xs-injectables-links-module-UserModule-1b1e0a24f74dd8d313757060077cb050275d06e096b7fe1aef351846cbb339f6e354db59cca90b72d7e52f5bd077196ed3a9418de5fadfa356db08c45b8539ea"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-1b1e0a24f74dd8d313757060077cb050275d06e096b7fe1aef351846cbb339f6e354db59cca90b72d7e52f5bd077196ed3a9418de5fadfa356db08c45b8539ea"' :
                                        'id="xs-injectables-links-module-UserModule-1b1e0a24f74dd8d313757060077cb050275d06e096b7fe1aef351846cbb339f6e354db59cca90b72d7e52f5bd077196ed3a9418de5fadfa356db08c45b8539ea"' }>
                                        <li class="link">
                                            <a href="injectables/UserRepository.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserRepository</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AdminCreateUserDto.html" data-type="entity-link" >AdminCreateUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/AdminGetUsersDto.html" data-type="entity-link" >AdminGetUsersDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/ChangePasswordUserDto.html" data-type="entity-link" >ChangePasswordUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateFileManagerBodyDto.html" data-type="entity-link" >CreateFileManagerBodyDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateFileManagerDto.html" data-type="entity-link" >CreateFileManagerDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateMessageDto.html" data-type="entity-link" >CreateMessageDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateOtpDto.html" data-type="entity-link" >CreateOtpDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateReferralCodeDto.html" data-type="entity-link" >CreateReferralCodeDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateRoleDto.html" data-type="entity-link" >CreateRoleDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateSessionDto.html" data-type="entity-link" >CreateSessionDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserAddressBodyDto.html" data-type="entity-link" >CreateUserAddressBodyDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserDto.html" data-type="entity-link" >CreateUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/EditUserDto.html" data-type="entity-link" >EditUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/FileManagerRepository.html" data-type="entity-link" >FileManagerRepository</a>
                            </li>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/NationalUserDto.html" data-type="entity-link" >NationalUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/Otp.html" data-type="entity-link" >Otp</a>
                            </li>
                            <li class="link">
                                <a href="classes/PaginationQueryStringDto.html" data-type="entity-link" >PaginationQueryStringDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/PassportUserDto.html" data-type="entity-link" >PassportUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/ReferralCode.html" data-type="entity-link" >ReferralCode</a>
                            </li>
                            <li class="link">
                                <a href="classes/Role.html" data-type="entity-link" >Role</a>
                            </li>
                            <li class="link">
                                <a href="classes/Session.html" data-type="entity-link" >Session</a>
                            </li>
                            <li class="link">
                                <a href="classes/SignInAuthDto.html" data-type="entity-link" >SignInAuthDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/SignUpAuthBodyDto.html" data-type="entity-link" >SignUpAuthBodyDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateMessageDto.html" data-type="entity-link" >UpdateMessageDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateOtpDto.html" data-type="entity-link" >UpdateOtpDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateReferralCodeDto.html" data-type="entity-link" >UpdateReferralCodeDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateRoleDto.html" data-type="entity-link" >UpdateRoleDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateSessionDto.html" data-type="entity-link" >UpdateSessionDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateUserAddressBodyDto.html" data-type="entity-link" >UpdateUserAddressBodyDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateUserDto.html" data-type="entity-link" >UpdateUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserEntity.html" data-type="entity-link" >UserEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/VerifiedBodyPhoneNumberDto.html" data-type="entity-link" >VerifiedBodyPhoneNumberDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/VerifiedHeaderEmailDto.html" data-type="entity-link" >VerifiedHeaderEmailDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/VerifyTwoFactorAuthentication.html" data-type="entity-link" >VerifyTwoFactorAuthentication</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#injectables-links"' :
                                'data-bs-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggingInterceptor.html" data-type="entity-link" >LoggingInterceptor</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TransformInterceptor.html" data-type="entity-link" >TransformInterceptor</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#guards-links"' :
                            'data-bs-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link" >AuthGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/AuthorizationGuard.html" data-type="entity-link" >AuthorizationGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#interfaces-links"' :
                            'data-bs-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/AccessTokenInterface.html" data-type="entity-link" >AccessTokenInterface</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response.html" data-type="entity-link" >Response</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});