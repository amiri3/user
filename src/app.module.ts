// BUILT IN MODULE
import { resolve } from 'path';

import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { ThrottlerModule } from '@nestjs/throttler';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import * as Joi from 'joi';

import { PrismaModule } from './prisma/prisma.module';
import { AuthModule } from './module/auth/auth.module';
import { UserModule } from './module/user/user.module';

import { jwtConstants } from './constants/jwt-constants';
import { OtpModule } from './module/otp/otp.module';
import { AuthGuard } from './guard/auth.guard';
import { RoleModule } from './module/role/role.module';
import { SessionModule } from './module/session/session.module';
import { ReferralCodeModule } from './module/referral-code/referral-code.module';
import { ScheduleModule } from '@nestjs/schedule';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { MessageModule } from './module/message/message.module';
import { FileManagerModule } from './module/file-manager/file-manager.module';
@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true,
      envFilePath: resolve('.env'),
      validationSchema: Joi.object({
        DATABASE_URL: Joi.string().required(),
      }),
    }),
    ThrottlerModule.forRoot([
      {
        ttl: Number(process.env.RATE_LIMIT_WINDOWS_MS) || 60000,
        limit: 10,
      },
    ]),
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
    }),
    ScheduleModule.forRoot(),
    PrismaModule,
    AuthModule,
    UserModule,
    OtpModule,
    RoleModule,
    SessionModule,
    ReferralCodeModule,
    EventEmitterModule.forRoot({
      wildcard: true,
      delimiter: '.',
    }),
    MessageModule,
    FileManagerModule
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {
  constructor() { }
}
