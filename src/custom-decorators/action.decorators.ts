import { SetMetadata } from '@nestjs/common';
export const ACTION_KEY = 'Action';
export const SetAction = (...actions) => SetMetadata(ACTION_KEY, actions);