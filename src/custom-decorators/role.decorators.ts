import { SetMetadata } from '@nestjs/common';
export const ROLE_KEY = 'Role';
export const SetRole = (...Role) => SetMetadata(ROLE_KEY, Role);