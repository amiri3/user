export interface AccessTokenInterface {
  user_id: number;
  is_active: boolean;
  ip: string;
  role_id : number;
}
