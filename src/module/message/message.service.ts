import { Inject, Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { ClientProxy } from '@nestjs/microservices';
import { map } from 'rxjs';

@Injectable()
export class MessageService implements OnApplicationBootstrap {
  constructor(
    @Inject('MESSAGE_SERVICE')
    private readonly messageMicroService: ClientProxy,
  ) {}
  async onApplicationBootstrap() {
    await this.messageMicroService.connect();
  }
  async create(
    createMessageDto: CreateMessageDto,
    userId: number,
  ): Promise<object> {
    const { body, is_public, title, users_id_access } = createMessageDto;

    const createMessageData = {
      body,
      is_public: is_public === 'true',
      title,
      users_id_access: is_public === 'true' ? [] : JSON.parse(users_id_access),
      user_id: userId,
    };
    return await this.messageMicroService.send<object>(
      { cmd: 'create_message' },
      createMessageData,
    );
  }

  async listMyMessage(userId: number): Promise<object> {
    return await this.messageMicroService.send<object[], number>(
      {
        cmd: 'my_message',
      },
      userId,
    );
  }

  async listMessagesForMe(userId: number): Promise<object> {
    return await this.messageMicroService.send<object[], number>(
      {
        cmd: 'message_for_me',
      },
      userId,
    );
  }
  findOne(id: number) {
    return `This action returns a #${id} message`;
  }

  update(id: number, updateMessageDto: UpdateMessageDto) {
    return `This action updates a #${id} message`;
  }

  remove(id: number) {
    return `This action removes a #${id} message`;
  }
}
