import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { MessageService } from './message.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { Public } from 'src/custom-decorators/public.decorators';
import { User } from 'src/custom-decorators/user.decorators';
import { AccessTokenInterface } from 'src/interface/access-token.interface';
import { FilesInterceptor } from '@nestjs/platform-express';

@Controller('message')
export class MessageController {
  constructor(private readonly messageService: MessageService) {}

  @Post('/')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(FilesInterceptor('attachs'))
  async create(
    @Body() createMessageDto: CreateMessageDto,
    @User() user: AccessTokenInterface,
    @UploadedFiles() attachs: Array<Express.Multer.File>,
  ): Promise<object> {
    const userId = user.user_id;
    return await this.messageService.create(createMessageDto, userId);
  }

  @Get('/my')
  @HttpCode(HttpStatus.OK)
  async listMyMessages(@User() user: AccessTokenInterface): Promise<object> {
    const userId = user.user_id;
    return await this.messageService.listMyMessage(userId);
  }

  @Get('for-me')
  async listMessageForMe(@User() user: AccessTokenInterface): Promise<object> {
    const userId = user.user_id;
    return await this.messageService.listMessagesForMe(userId);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMessageDto: UpdateMessageDto) {
    return this.messageService.update(+id, updateMessageDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.messageService.remove(+id);
  }
}
