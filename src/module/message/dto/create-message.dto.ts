import { IsBooleanString, IsNotEmpty, IsString } from 'class-validator';

export class CreateMessageDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  body: string;

  @IsNotEmpty()
  @IsString()
  users_id_access: string;

  @IsNotEmpty()
  @IsBooleanString()
  is_public: string;
}
