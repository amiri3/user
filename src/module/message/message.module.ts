import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'MESSAGE_SERVICE',
        transport: Transport.TCP,
        options: {
          port: Number(process.env.MICROSERVICE_PORT) || 3001,
          host: process.env.MICROSERVICE_HOST || '0.0.0.0',
        },
      },
    ]),
  ],

  controllers: [MessageController],
  providers: [MessageService],
})
export class MessageModule {}
