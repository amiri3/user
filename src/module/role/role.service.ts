import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class RoleService {
  private readonly roleRepository: any;
  constructor(private readonly prismaService: PrismaService) {}

  async findOneByIdAndUserRole(id: number) {
    const role = await this.roleRepository.findOneRoleById(id);
    if (!role) {
      throw new NotFoundException('نقش مورد نظر در سامانه موجود نمی باشد.');
    }
    if (!role?.['user_role']) {
      throw new BadRequestException('نقش مورد نظر قابل استفاده نمی باشد.');
    }
    return role;
  }

  async findOneByNameAndUserRole(name: string) {
    const role = await this.prismaService.role.findFirst({
      where: {
        name,
      },
      select: {
        id: true,
      },
    });
    if (!role) {
      throw new NotFoundException('نقش مورد نظر در سامانه موجود نمی باشد.');
    }
    return role;
  }

  async findOneRoleById(id: number) {
    const role = await this.prismaService.role.findFirst({
      where: {
        id,
      },
    });
    if (!role) {
      throw new NotFoundException('نقش مورد نظر در سامانه موجود نمی باشد.');
    }
    return role;
  }
  async roleAction(role_id: number) {
    const actions = await this.prismaService.role_Action.findMany({where  : {role_id } })
    return actions;
  }

  async findExchangeAclByUserIdAndActionMethod(
    userId: number,
    actionMethod: string,
  ): Promise<object | null> {
    const exchangeAcl =
      await this.roleRepository.findOneExchangeAclByUserIdAndActionMethod(
        userId,
        actionMethod,
      );
    return exchangeAcl;
  }
}
