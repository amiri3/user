import {
  IsNotEmpty,
  MinLength,
  IsPhoneNumber,
  IsString,
  IsEnum,
} from 'class-validator';
import { Roles } from 'src/enum/role.enum';

export class SignUpAuthBodyDto {
  @IsNotEmpty()
  first_name: string;

  @IsNotEmpty()
  last_name: string;

  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @IsNotEmpty()
  @IsPhoneNumber()
  @IsString()
  phone_number: string;

  @IsNotEmpty()
  @IsEnum([Roles.USER])
  role_name: Roles;
}

export class VerifiedBodyPhoneNumberDto {
  @IsNotEmpty()
  @IsString()
  code: string;
}

export class VerifiedHeaderEmailDto {
  @IsNotEmpty()
  @IsString()
  authorization: string;
}
