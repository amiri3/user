import { IsNotEmpty, IsEmail, MinLength, IsPhoneNumber, IsString, IsNumber, IsJSON, IsObject, IsEnum } from "class-validator";
// import { Two_Step_Verification_Type } from "src/enum/main.enum";


export class SignInAuthDto {
   @IsString()
   @IsPhoneNumber()
   phone_number: string

   @IsNotEmpty()
   @IsString()
   password: string
}
export class VerifyTwoFactorAuthentication {
   @IsNotEmpty()
   @IsString()
   code: string;


   // @IsNotEmpty()
   // @IsEnum(Two_Step_Verification_Type)
   // type: Two_Step_Verification_Type
}