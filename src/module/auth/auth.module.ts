import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';

import { UserModule } from "../user/user.module";
import { OtpModule } from "../otp/otp.module";
import { PrismaModule } from 'src/prisma/prisma.module';
import { RoleModule } from "../role/role.module";
import { SessionModule } from '../session/session.module';

@Module({
  imports: [UserModule, OtpModule, PrismaModule, SessionModule, RoleModule],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AuthModule { }
