import {
  Injectable,
  Logger,
  NotFoundException,
  BadRequestException,
  UnauthorizedException,
} from '@nestjs/common';
import {
  SignUpAuthBodyDto,
  VerifiedBodyPhoneNumberDto,
} from './dto/signup-auth.dto';
import { SignInAuthDto } from './dto/signin-auth.dto';
import { UserService } from '../user/user.service';
import { OtpService } from '../otp/otp.service';
import { RoleService } from '../role/role.service';
import { SessionService } from 'src/module/session/session.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';
import { AccessTokenInterface } from 'src/interface/access-token.interface';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    private readonly userService: UserService,
    private readonly otpService: OtpService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly sessionService: SessionService,
    private readonly roleService: RoleService,
  ) {}
  async signup(signUpAuthDto: SignUpAuthBodyDto) {
    const { first_name, last_name, phone_number, password, role_name } =
      signUpAuthDto;
    const userByPhoneNumber =
      await this.userService.findUniqueUserByPhoneNumber(phone_number);
    if (userByPhoneNumber && userByPhoneNumber.is_active) {
      throw new BadRequestException(
        'کاربری با این مشخصات در سامانه موجود می باشد.',
      );
    }
    const role = await this.roleService.findOneByNameAndUserRole(role_name);
    const bcryptSalt = bcrypt.genSaltSync(10);
    const hashPassword = bcrypt.hashSync(password, bcryptSalt);
    let user;
    if (!userByPhoneNumber) {
      user = await this.userService.createUserNotActive({
        first_name,
        last_name,
        username: first_name + last_name,
        password: hashPassword,
        phone_number,
        role_id: Number(role.id),
      });
    } else if (userByPhoneNumber && !userByPhoneNumber.is_active) {
      user = await this.userService.findUniqueUserByPhoneNumber(phone_number);
    }
    const otpCode: number = await this.otpService.generateOtpCode();
    const otpExpiredTime = new Date(Date.now() + 185000);
    const userId = user.id;
    const expiredAllOtpCodeByUserId =
      await this.otpService.updateExpiredOtpCodeByUserId(userId);
    const createOtp = await this.otpService.create({
      code: otpCode.toString(),
      user_id: userId,
      expired_at: otpExpiredTime,
    });
    const jwtPayload = {
      user_id: userId,
      is_active: false,
    };
    const jwt = await this.jwtService.signAsync(jwtPayload, {
      expiresIn: '180s',
    });
    return {
      access_token: jwt,
    };
  }
  async retrySendVerifiedOtpCode(phoneNumber: string) {
    const user = await this.userService.findUniqueUserByPhoneNumber(
      phoneNumber,
      true,
    );
    const userId = user.id;
    if (user.is_active) {
      throw new BadRequestException('شماره تلفن کاربر مورد نظر تایید شده است.');
    }
    const otpCode: number = await this.otpService.generateOtpCode();
    const otpExpiredTime = new Date(Date.now() + 185000);
    const expiredAllOtpCodeByUserId =
      await this.otpService.updateExpiredOtpCodeByUserId(userId);
    const createOtp = await this.otpService.create({
      code: otpCode.toString(),
      user_id: userId,
      expired_at: otpExpiredTime,
    });
    return {
      message: 'کد تایید شماره تلفن ارسال شد.',
    };
  }

  async verifiedPhoneNumber(
    verifiedBodyPhoneNumberDto: VerifiedBodyPhoneNumberDto,
    userId: number,
    headers: object,
  ) {
    const { code } = verifiedBodyPhoneNumberDto;
    const user = await this.userService.findUniqueUserByUserId(userId, true);
    if (user.is_active) {
      throw new BadRequestException(
        'ایمیل و رمز عبور کاربر مورد نظر تایید شده است.',
      );
    }
    if (this.configService.get<string>('MODE') == 'DEV' && code != '1111') {
      const findCode = await this.otpService.findOneByCodeAndUserId(
        code,
        userId,
      );
      if (Number(findCode.expired_at) < new Date().getTime()) {
        throw new BadRequestException('کد ارسال شده منقضی شده است.');
      }
      if (findCode.used || findCode.expired) {
        throw new BadRequestException('کد ارسال شده منقضی شده است.');
      }
    }
    const verifyPhoneNumberByUserId =
      await this.userService.verifyPhoneNumberByUserId(userId);
    if (this.configService.get<string>('MODE') == 'DEV' && code != '1111') {
      const updateOtpUsed = await this.otpService.updateUsedOtpCode(
        code,
        userId,
      );
    }
    const refrehs_token_jwtPayload = { user_id: user?.['id'] };
    const access_token_jwtPayload = {
      user_id: userId,
      is_active: verifyPhoneNumberByUserId.is_active,
      // role: user?.['role_id'],
      ip: headers['ip'],
    };
    const refresh_token = await this.jwtService.signAsync(
      refrehs_token_jwtPayload,
      { expiresIn: '7D' },
    );
    const access_token = await this.jwtService.signAsync(
      access_token_jwtPayload,
      { expiresIn: '300s' },
    );
    const sessionExpireTime = new Date();
    sessionExpireTime.setDate(sessionExpireTime.getDate() + 7);
    const addSession = await this.sessionService.create({
      device: headers,
      refresh_token,
      expired_time: sessionExpireTime,
      user_id: userId,
    });
    return {
      access_token,
      refresh_token,
    };
  }

  async signin(signInAuthDto: SignInAuthDto, headers: object) {
    const { phone_number, password } = signInAuthDto;
    const user = await this.userService.findUniqueUserByPhoneNumber(
      phone_number,
      true,
    );
    if (!user.is_active) {
      throw new NotFoundException('کاربر مورد نظر در سامانه موجود نمی باشد.');
    }
    const checkPasword = await this.userService.comparePassword(
      password,
      user?.['password'],
    );
    if (!checkPasword) {
      throw new BadRequestException('مشخصات ارسال شده صحیح نمی باشد');
    }
    const userId = user.id;
    const refresh_token_jwtPayload = { user_id: userId };
    const access_token_jwtPayload: AccessTokenInterface = {
      user_id: userId,
      is_active: user.is_active,
      ip: headers['ip'],
      role_id: +user.role_id,
    };
    const mode = await this.configService.get<string>('MODE');
    const access_token = await this.jwtService.signAsync(
      access_token_jwtPayload,
      { expiresIn: mode === 'DEV' ? '1D' : '300s' },
    );
    const refresh_token = await this.jwtService.signAsync(
      refresh_token_jwtPayload,
      { expiresIn: '7D' },
    );
    const sessionExpireTime = new Date();
    sessionExpireTime.setDate(sessionExpireTime.getDate() + 7);
    const addSession = await this.sessionService.create({
      device: headers,
      refresh_token,
      expired_time: sessionExpireTime,
      user_id: userId,
    });
    return {
      access_token,
      refresh_token,
    };
  }

  async refreshToken(user_id: number, refreshToken: string, ip: string) {
    const user = await this.userService.findUniqueUserByUserId(user_id, true);
    const now = Date.now();
    const findSessionByRefreshToken =
      await this.sessionService.findSessionByRefreshToken(refreshToken);
    if (!findSessionByRefreshToken) {
      throw new NotFoundException(
        'ورود کاربر مورد نظر در سامانه موجود نمی‌باشد.',
      );
    }
    if (new Date(findSessionByRefreshToken.expired_time).getTime() < now) {
      throw new UnauthorizedException('توکن ارسال شده منقضی می باشد.');
    }
    if (findSessionByRefreshToken.logout) {
      throw new UnauthorizedException('توکن ارسال شده منقضی می باشد.');
    }
    const access_token_jwtPayload: AccessTokenInterface = {
      user_id: user?.['id'],
      ip,
      is_active: user.is_active,
      role_id: +user.role_id,
    };
    const access_token = await this.jwtService.signAsync(
      access_token_jwtPayload,
      { expiresIn: '1D' },
    );
    return {
      access_token,
    };
  }

  async logOut(user_id: number, refreshToken: string) {
    const user = await this.userService.findUniqueUserByUserId(user_id, true);
    const session =
      await this.sessionService.findSessionByRefreshToken(refreshToken);
    if (session) {
      const logoutSession = await this.sessionService.findSessionByIdAndLogout(
        session?.id,
      );
    }
    return {
      message: 'خروج با موفقیت انجام شد.',
    };
  }
}
