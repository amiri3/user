import { Controller, Get, Post, Body, Patch, Param, Headers, UseInterceptors, Put, Req, HttpCode, HttpStatus, Request, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignUpAuthBodyDto, VerifiedBodyPhoneNumberDto, VerifiedHeaderEmailDto } from './dto/signup-auth.dto';
import { SignInAuthDto, VerifyTwoFactorAuthentication } from "./dto/signin-auth.dto";
import { Public } from "../../custom-decorators/public.decorators";




@Controller('/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Public()
  @Post('/signup')
  @HttpCode(HttpStatus.OK)
  signUp(@Body() signUpAuthDto: SignUpAuthBodyDto) {
    return this.authService.signup(signUpAuthDto);
  }

  @Public()
  @Post('/signin')
  @HttpCode(HttpStatus.OK)
  signin(@Body() signInAuthDto: SignInAuthDto, @Headers() headers, @Req() request: Request) {
    headers['ip'] = request['ip'];
    return this.authService.signin(signInAuthDto, headers);
  }


  @Put('/verify/phone-number')
  @HttpCode(HttpStatus.OK)
  verifiedEmail(@Body() verifiedBodyPhoneNumberDto: VerifiedBodyPhoneNumberDto, @Req() request) {
    const userId = request?.user.user_id;
    let headers = request?.headers;
    headers['ip'] = request['ip'];
    return this.authService.verifiedPhoneNumber(verifiedBodyPhoneNumberDto, userId, headers)
  }


  @Public()
  @Put('/email/retry-code')
  @HttpCode(HttpStatus.OK)
  retrySendVerifiedEmailCode(@Body('phone_number') phoneNumber : string) {
    return this.authService.retrySendVerifiedOtpCode(phoneNumber);
  }

  @Put('/refresh')
  @HttpCode(HttpStatus.OK)
  refreshToken(@Req() request) {
    const userId = request?.user?.user_id;
    const refreshToken = request?.authorization;
    const ip: string = request["ip"]
    return this.authService.refreshToken(userId, refreshToken, ip);
  }

  @Get('/logout')
  @HttpCode(HttpStatus.OK)
  logout(@Req() request) {
    const userId = request?.user.user_id;
    const refreshToken = request?.authorization;
    return this.authService.logOut(userId, refreshToken);
  }

}
