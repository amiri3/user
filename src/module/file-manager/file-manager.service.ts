import { ForbiddenException, Injectable, Logger, NotFoundException, OnModuleInit } from "@nestjs/common";
import { File } from "@prisma/client";

import { v4 as uuidV4 } from "uuid"



import * as FileSystem from "node:fs";
import * as Path from "node:path";
import { FileManagerRepository } from "./file-manager.repository";
import { CreateFileManagerBodyDto, CreateFileManagerDto } from "./dto/create-file-manager.dto";
import { ConfigService } from "@nestjs/config";
import { PrismaService } from "src/prisma/prisma.service";
@Injectable()
export class FileManagerService implements OnModuleInit {

    private readonly logger = new Logger(FileManagerService.name)

    async onModuleInit() {
        const checkUploadFolderIsAvalible = FileSystem.existsSync(Path.resolve('./upload'))
        if (!checkUploadFolderIsAvalible) {
            const createUploadFolder = FileSystem.mkdirSync(Path.resolve('./upload'))
        }
    }

    constructor(
        private readonly fileManagerRepository: FileManagerRepository,
        private readonly prismaService: PrismaService
    ) {

    }

    async uploadFile(files: Array<Express.Multer.File>, createFileManagerBodyDto: CreateFileManagerBodyDto, userId: number): Promise<Array<File>> {
        const createFileManagers = [];
        let { is_public, users_access } = createFileManagerBodyDto;
        let isPublic = is_public.toLocaleLowerCase() === "true" ? true : false;
        let usersAccess = JSON.parse(users_access);
        for (let index = 0; index < files.length; index++) {
            const uuid = this.unquieFileUuidName();
            const { buffer, mimetype, size } = files[index];
            const filePath = `${Path.resolve('./upload')}/${uuid}.${mimetype.split('/')[1]}`;
            const writeFileStream = await FileSystem.writeFileSync(filePath, buffer);
            const createFileManager = await this.createFileManagerRepositor({
                is_public: isPublic,
                link: filePath,
                size,
                type: mimetype,
                user_id: userId,
                users_access: usersAccess
            })
            createFileManagers.push(createFileManager);
        }
        return createFileManagers
    }


    async listMyFiles(userId: number): Promise<Array<File>> {
        const files = await this.prismaService.file.findMany({
            where: {
                user_id: userId
            }
        })
        return files;
    }

    async listFilesAccessedMe(userId: number): Promise<Array<File>> {
        const files = await this.prismaService.file.findMany({
            where: {
                users_access: {
                    has: userId
                }
            }
        })
        return files;
    }
    async serveFile(fileId: string, userId: number): Promise<string> {
        const file = await this.prismaService.file.findUnique({
            where: {
                id: fileId
            }
        })
        if (!file) {
            throw new NotFoundException("فایل مورد نظر در سامانه موجود نمی باشد.");
        }
        if (!file.is_public && (!file.users_access.find(id => id === userId))) {
            throw new ForbiddenException("شما دسترسی به این فایل را ندارید.");
        }
        const readFileSync = FileSystem.readFileSync(file.link).toString('base64')
        return readFileSync
    }


    listUplaodFileFolderFiles(): string[] {
        const uploadFolderContent = FileSystem.readdirSync(Path.resolve('./upload'))
        return uploadFolderContent
    }

    unquieFileUuidName(): string {
        const mapedListUplaodFileFolderFiles = this.listUplaodFileFolderFiles().map(name => name.split('.')[0]);
        const generateUuid = (mapedListUplaodFileFolderFiles: string[]) => {
            const uuid = uuidV4();
            if (mapedListUplaodFileFolderFiles.includes(uuid)) {
                generateUuid(mapedListUplaodFileFolderFiles)
            } else {
                return uuid
            }
        }
        return generateUuid(mapedListUplaodFileFolderFiles)
    }


    async createFileManagerRepositor(createFileManagerDto: CreateFileManagerDto): Promise<File> {
        try {
            const createFileManager = await this.prismaService.file.create({
                data: createFileManagerDto
            })
            return createFileManager
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }


}