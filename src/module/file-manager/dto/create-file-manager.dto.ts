import { IsArray, IsBoolean, IsBooleanString, IsNotEmpty, IsNumber, IsString, ValidateNested } from "class-validator";

export class CreateFileManagerBodyDto {


    @IsNotEmpty()
    @IsBooleanString()
    is_public: string;

    @IsNotEmpty()
    @IsString()
    users_access: string
}

export class CreateFileManagerDto {
    @IsNotEmpty()
    @IsNumber()
    user_id: number;

    @IsNotEmpty()
    @IsString()
    link: string;

    @IsNotEmpty()
    @IsNumber()
    size: number;

    @IsNotEmpty()
    @IsString()
    type: string;

    @IsNotEmpty()
    @IsBoolean()
    is_public: boolean

    @IsNotEmpty()
    @IsArray()
    users_access: number[]
}