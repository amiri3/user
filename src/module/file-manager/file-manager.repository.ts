import { Logger } from "@nestjs/common";
import { File } from "@prisma/client";
import { PrismaService } from "src/prisma/prisma.service";
import { CreateFileManagerDto } from "./dto/create-file-manager.dto";

export class FileManagerRepository {
    private readonly logger = new Logger(FileManagerRepository.name);
    constructor(
        private readonly prismaService: PrismaService
    ) {

    }


    async createFileManager(createFileManagerDto: CreateFileManagerDto): Promise<File> {
        try {
            const createFileManager = await this.prismaService.file.create({
                data: createFileManagerDto
            })
            return createFileManager
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }
}