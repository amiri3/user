import { Body, Controller, FileTypeValidator, Get, HttpCode, HttpStatus, MaxFileSizeValidator, Param, ParseFilePipe, Post, UploadedFiles, UseInterceptors } from "@nestjs/common";
import { FileManagerService } from "./file-manager.service";
import { FilesInterceptor } from "@nestjs/platform-express";
import { CreateFileManagerBodyDto } from "./dto/create-file-manager.dto";
import { User } from "src/custom-decorators/user.decorators";
import { AccessTokenInterface } from "src/interface/access-token.interface";
import { File } from "@prisma/client";


@Controller("/file-manager")
export class FileManagerController {

    constructor(
        private readonly fileManagerService: FileManagerService
    ) {

    }



    @Post('/upload')
    @UseInterceptors(
        FilesInterceptor('files', 5)
    )
    @HttpCode(HttpStatus.OK)
    async uploadFile(
        @Body() createFileManagerBodyDto: CreateFileManagerBodyDto,
        @UploadedFiles(
            new ParseFilePipe({
                validators: [
                    new MaxFileSizeValidator({ maxSize: 1000000, message: "حجم فایل ارسالی بیش از حد مجاز می باشد." }),
                    // new FileTypeValidator({ fileType: 'image/jpeg' })
                ]
            })
        ) files: Array<Express.Multer.File>,
        @User() user: AccessTokenInterface
    ): Promise<object> {
        const userId = user.user_id;
        return await this.fileManagerService.uploadFile(files, createFileManagerBodyDto, userId);
    }

    @Get('/my')
    @HttpCode(HttpStatus.OK)
    async listMyFiles(@User() user: AccessTokenInterface,): Promise<Array<File>> {
        const files = await this.fileManagerService.listMyFiles(user.user_id)
        return files
    }

    @Get('/accessd-me')
    @HttpCode(HttpStatus.OK)
    async listFilesAccessedMe(@User() user: AccessTokenInterface,): Promise<Array<File>> {
        const files = await this.fileManagerService.listFilesAccessedMe(user.user_id)
        return files
    }
    @Get('/serve/:fileId')
    @HttpCode(HttpStatus.OK)
    async serveFile(@Param('fileId') fileId: string, @User() user: AccessTokenInterface): Promise<string> {
        const userId = user.user_id;
        return await this.fileManagerService.serveFile(fileId, userId)
    }
}