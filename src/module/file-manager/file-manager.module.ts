import { Module } from "@nestjs/common";
import { FileManagerController } from "./file-manager.controller";
import { FileManagerService } from "./file-manager.service";
import { FileManagerRepository } from "./file-manager.repository";
import { PrismaModule } from "src/prisma/prisma.module";


@Module({
    imports: [PrismaModule],
    controllers: [FileManagerController],
    providers: [FileManagerService, FileManagerRepository],
    exports: [FileManagerService]
})
export class FileManagerModule {

}