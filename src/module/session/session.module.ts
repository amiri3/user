import { Module } from '@nestjs/common';
import { SessionService } from './session.service';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  exports: [SessionService],
  imports: [PrismaModule],
  providers: [SessionService],
  controllers: []
})
export class SessionModule { }
