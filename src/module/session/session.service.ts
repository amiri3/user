import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { CreateSessionDto } from './dto/create-session.dto';
import { UpdateSessionDto } from './dto/update-session.dto';
import { PrismaService } from 'src/prisma/prisma.service';


@Injectable()
export class SessionService {
  private readonly sessionRepository: any
  constructor(
    private readonly prismaService: PrismaService
  ) { }
  async create(createSessionDto: CreateSessionDto) {
    const { device, expired_time, refresh_token, user_id } = createSessionDto;
    const useSessionsCount = await this.prismaService.session.count({
      where: {
        user_id,
        expired_time: {
          gte: new Date()
        },
        logout: false,
      },
    })
    if (useSessionsCount >= 3) {
      throw new BadRequestException("ورود مجاز نمی باشد حداکثر دستگاه های مجاز به ورود 3 عدد می باشد");
    }
    const addSession = await this.prismaService.session.create({
      data: {
        user_id,
        refresh_token,
        expired_time,
        device
      }
    })
    return addSession
  }
  findAll() {
    return `This action returns all session`;
  }
  async findOne(query: object) {
    const session = await this.sessionRepository.findOne(query);
    if (!session) {
      throw new NotFoundException('سشن مورد نظر در سامانه موجود نمی باشد');
    }
    return session
  }
  async findSessionByRefreshToken(refreshToken: string) {
    const session = await this.prismaService.session.findFirst({ where: { refresh_token: refreshToken } });
    return session
  }
  async findSessionByIdAndLogout(id: number) {
    const session = await this.prismaService.session.findFirst({ where: { id } });
    if (session) {
      const updateSession = await this.prismaService.session.update({ where: { id }, data: { logout: true } });
      return updateSession
    }
  }

  
}
