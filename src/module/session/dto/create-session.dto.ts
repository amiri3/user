import { IsNotEmpty, IsString, IsNumber, IsJSON, IsDate, IsObject } from "class-validator";

export class CreateSessionDto {
   @IsNotEmpty()
   @IsObject()
   device: object;

   @IsNotEmpty()
   @IsString()
   refresh_token: string;

   @IsNotEmpty()
   @IsDate()
   expired_time: Date;

   @IsNotEmpty()
   @IsNumber()
   user_id: number
}
