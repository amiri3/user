import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateOtpDto } from './dto/create-otp.dto';
import { UpdateOtpDto } from './dto/update-otp.dto';


import * as randomNumber from "random-number";
import { ConfigService } from '@nestjs/config';

import * as qrCode from "qrcode";
import * as speakeasy from "speakeasy";
import * as OTPAuth from "otpauth";
import { PrismaService } from '../../prisma/prisma.service';

@Injectable()
export class OtpService {
  private readonly otpRepository: any
  constructor(
    private readonly configService: ConfigService,
    private readonly prismaService: PrismaService
  ) {
  }
  async generateOtpCode(): Promise<number> {
    const listAvalibleOfOtpCode = await this.allOtpAvalibaleCode();
    const genrateRandomCode = () => {
      const random = randomNumber({
        min: 1000,
        max: 10000,
        integer: true
      })
      if (listAvalibleOfOtpCode.includes(random)) {
        genrateRandomCode();
      } else {
        return random
      }
    }
    return genrateRandomCode();
  }

  async allOtpAvalibaleCode(): Promise<Array<number>> {
    const allCodes = await this.prismaService.otp_Code.findMany({ select: { code: true } });
    const allCodesMaped = allCodes.map((code) => Number(code));
    return allCodesMaped
  }
  async create(createOtpDto: CreateOtpDto) {
    const { code, user_id, expired_at } = createOtpDto;
    const checkCode = await this.prismaService.otp_Code.findFirst({
      where: {
        code
      },
      select: {
        id: true
      }
    })
    if (checkCode) {
      throw new BadRequestException('کد otp موجود می باشد.');
    }
    const addOtp = await this.prismaService.otp_Code.create({ data: { code, user_id, expired_at } });
    return addOtp
  }
  async findOneByCodeAndUserId(code: string, userId: number) {
    const otpByUserIdAndCode = await this.prismaService.otp_Code.findFirst({
      where: {
        code,
        user_id: userId
      }
    })
    if (!otpByUserIdAndCode) {
      throw new NotFoundException("کد تایید شماره تلفن ارسال شده در سامانه موجود نمی باشد.");
    }
    return otpByUserIdAndCode
  }
  async updateUsedOtpCode(code: string, userId: number) {
    const otpByUserIdAndCodeUpdate = await this.prismaService.otp_Code.update({
      where: {
        user_id: userId,
        code
      },
      data: {
        used: true
      }
    })
    if (!otpByUserIdAndCodeUpdate) {
      throw new NotFoundException("کد تایید شماره تلفن ارسال شده در سامانه موجود نمی باشد.");
    }
    return otpByUserIdAndCodeUpdate
  }
  async updateExpiredOtpCodeByUserId(user_id: number) {
    const otpByUserIdUpdate = await this.prismaService.otp_Code.updateMany({
      where: {
        user_id
      },
      data: {
        expired: true,
        expired_at: new Date()
      }
    })
    return otpByUserIdUpdate
  }
}
