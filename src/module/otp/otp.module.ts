import { Module } from '@nestjs/common';
import { OtpService } from './otp.service';
import { OtpController } from './otp.controller';
import { PrismaModule } from '../../prisma/prisma.module';
// import { CrudManagerModule } from "../crud-manager/crud-manager.module";
// import { OtpRepository } from "./otp.repository";
@Module({
  imports: [PrismaModule],
  exports: [OtpService],
  controllers: [OtpController],
  providers: [OtpService],
})
export class OtpModule { }
