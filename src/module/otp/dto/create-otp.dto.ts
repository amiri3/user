import { IsDateString, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class CreateOtpDto {
   @IsNotEmpty()
   @IsString()
   code: string
   @IsNotEmpty()
   @IsNumber()
   user_id: number


   @IsNotEmpty()
   @IsDateString()
   expired_at
}
