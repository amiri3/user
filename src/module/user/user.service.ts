import {
  Injectable,
  BadRequestException,
  NotFoundException,
  Logger,
} from '@nestjs/common';

import * as bcrypt from 'bcrypt';



import { ChangePasswordUserDto } from './dto/change-password-user.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { UserEntity } from './entities/user.entity';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);
  constructor(
    private readonly userRepository: UserRepository,
    private readonly prismaService: PrismaService,
  ) {}
  async findUniqueUserByPhoneNumber(
    phoneNumber: string,
    notAllowNull?: Boolean,
  ): Promise<UserEntity | null> {
    const user = await this.prismaService.user.findUnique({
      where: {
        phone_number: phoneNumber,
      },
    });
    if (notAllowNull && !user) {
      throw new NotFoundException('کاربر مورد نظر در سامانه موجود نمی‌باشد.');
    }
    return user;
  }
  async findUniqueUserByUserId(
    userId: number,
    notAllowNull?: Boolean,
  ): Promise<UserEntity | null> {
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
    });
    if (notAllowNull && !user) {
      throw new NotFoundException('کاربر مورد نظر در سامانه موجود نمی‌باشد.');
    }
    return user;
  }
  async createUserNotActive(
    data: Omit<
      UserEntity,
      'id' | 'created_at' | 'updated_at' | 'is_admin' | 'is_active'
    >,
  ): Promise<UserEntity> {
    const createUser = await this.prismaService.user.create({
      data: {
        ...data,
        is_admin: false,
        is_active: false,
      },
    });
    return createUser;
  }
  async verifyPhoneNumberByUserId(userId: number): Promise<UserEntity | null> {
    const updateUser = await this.prismaService.user.update({
      where: {
        id: userId,
      },
      data: {
        is_active: true,
      },
    });
    return updateUser;
  }
  public comparePassword(currentPassword, hashPassword) {
    try {
      const comparePassword = bcrypt.compareSync(currentPassword, hashPassword);
      return comparePassword;
    } catch (error) {
      return false;
    }
  }
  async changePassword(
    userId: number,
    changePasswordUserDto: ChangePasswordUserDto,
  ) {
    if (
      changePasswordUserDto.new_password !==
      changePasswordUserDto.retype_new_password
    ) {
      throw new BadRequestException(
        'فیلدهای رمز عبور جدید و تکرار رمز عبور جدید یکسان نمی باشد.',
      );
    }
    const user = await this.findUniqueUserByUserId(userId, true);
    const checkPassword = await this.comparePassword(
      changePasswordUserDto.current_password,
      user.password,
    );
    if (!checkPassword) {
      throw new BadRequestException('رمز عبور صحیح نمی باشد.');
    }
    const genSaltSync = bcrypt.genSaltSync(10);
    const hashPassword = bcrypt.hashSync(
      changePasswordUserDto.new_password,
      genSaltSync,
    );
    let updatePassword = await this.userRepository.update(userId, {
      password: hashPassword,
    });

    return { message: 'رمز عبور جدید با موفقیت ویرایش شد.' };
  }

}
