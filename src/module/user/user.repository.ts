// src/user/user.repository.ts

import { User } from '@prisma/client';
import { Injectable, Logger } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class UserRepository {
  private readonly logger = new Logger(UserRepository.name);
  constructor(private readonly prismaService: PrismaService) {}

  async create(data): Promise<User> {
    try {
      return this.prismaService.user.create({
        data,
      });
    } catch (error) {
      this.logger.error(error);
      return null;
    }
  }

  async update(id: number, data): Promise<User> {
    try {
      return this.prismaService.user.update({
        where: { id },
        data,
      });
    } catch (error) {
      this.logger.error(error);
      return null;
    }
  }
}
