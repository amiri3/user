

import { IsEmail, IsEnum, IsNotEmpty, IsNumber, IsString, MinLength, IsDateString} from "class-validator";

export class EditUserDto {

   @IsNotEmpty()
   @IsString()
   first_name: string;

   @IsNotEmpty()
   @IsString()
   last_name: string;


   @IsNotEmpty()
   @IsString()
   national_code: string


   @IsNotEmpty()
   @IsString()
   passport_code: string


   @IsNotEmpty()
   @IsDateString()
   birth_date: Date

}
