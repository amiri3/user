import { IsOptional, IsString } from "class-validator";

export class AdminGetUsersDto {
  @IsOptional()
  @IsString()
  last_name: string;

  @IsOptional()
  @IsString()
  phone_number: string;
}