

import { IsEmail, IsEnum, IsNotEmpty, IsNumber, IsString, MinLength, } from "class-validator";

export class CreateUserDto {

   @IsNotEmpty()
   @IsString()
   first_name: string;

   @IsNotEmpty()
   @IsString()
   last_name: string;


   @IsNotEmpty()
   @IsString()
   phone_number: string

   @IsNotEmpty()
   @IsEmail()
   email: string

   @IsNotEmpty()
   @IsString()
   password: string

   @IsNotEmpty()
   @IsNumber()
   role_id: number


   @IsNotEmpty()
   @IsString()
   referral_code: string

}
