

import { IsEmail, IsEnum, IsNotEmpty, IsNumber, IsString, MinLength, IsDateString} from "class-validator";

export class ChangePasswordUserDto {

   @IsNotEmpty()
   @IsString()
   current_password: string;

   @IsNotEmpty()
   @IsString()
   new_password: string;


   @IsNotEmpty()
   @IsString()
   retype_new_password: string

}
