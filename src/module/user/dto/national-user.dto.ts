

import { IsEmail, IsEnum, IsNotEmpty, IsNumber, IsString, MinLength, IsDateString} from "class-validator";

export class NationalUserDto {

   @IsNotEmpty()
   @IsString()
   first_name: string;

   @IsNotEmpty()
   @IsString()
   last_name: string;


   @IsNotEmpty()
   @IsString()
   national_code: string


   @IsNotEmpty()
   @IsDateString()
   birth_date: Date

   /*@IsNotEmpty()
   @IsString()
   national_image: string;

   @IsNotEmpty()
   @IsString()
   national_back_image: string;*/

}
