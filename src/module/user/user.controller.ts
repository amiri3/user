import {
  Controller,
  Post,
  Body,
  Put,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ChangePasswordUserDto } from './dto/change-password-user.dto';
import { User } from 'src/custom-decorators/user.decorators';
import { AccessTokenInterface } from 'src/interface/access-token.interface';

@Controller('/user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Put('/password/change')
  @HttpCode(HttpStatus.OK)
  async changePassword(
    @User() user: AccessTokenInterface,
    @Body() changePasswordUserDto: ChangePasswordUserDto,
  ): Promise<object> {
    const { user_id: userId } = user;
    const result = await this.userService.changePassword(
      userId,
      changePasswordUserDto,
    );
    return result;
  }
}
