import { User_Gender } from "@prisma/client"

export class UserEntity {
  id: number
  username: string
  avatar?: string
  phone_number: string
  password: string
  first_name: string
  last_name: string
  info?: string
  is_active?: boolean
  is_admin?: boolean
  email?: string
  national_code?: string
  education?: string
  current_job?: string
  gender?: User_Gender
  is_married?: boolean
  birthday?: Date;
  role_id?: number;
  created_at: Date
  updated_at: Date
}