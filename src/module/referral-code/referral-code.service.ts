import {
  Injectable,
  NotAcceptableException,
  BadRequestException,
  NotFoundException,
  Logger,
} from '@nestjs/common';
import { UpdateReferralCodeDto } from './dto/update-referral-code.dto';

import { ReferralCodeRepository } from './referral-code.repository';
import { UserService } from '../user/user.service';

import * as randomstring from 'randomstring';
import { paginationSkip } from 'src/helper/pagination.helper';
import { ReferralCodeCharsetEnum } from 'src/enum/charset.enum';

@Injectable()
export class ReferralCodeService {
  private readonly logger = new Logger(ReferralCodeService.name);
  constructor(
    private readonly referralCodeRepository: ReferralCodeRepository,
    private readonly userService: UserService,
  ) {}
  async listOfAvalibaleReferralCodes(): Promise<Array<string>> {
    try {
      const referralCodes =
        await this.referralCodeRepository.listOfAvalibaleReferralCodes();
      const mapedReferralCodes: string[] = referralCodes.map(
        (referralCode) => referralCode['code'],
      );
      return mapedReferralCodes;
    } catch (error) {
      this.logger.error(error);
      return [];
    }
  }

  async uniqueReferralCode(): Promise<string> {
    const listOfAvalibaleReferralCodes =
      await this.listOfAvalibaleReferralCodes();
    const generateCode = async () => {
      const pureReferralCode: string = await this.pureReferralCode({
        charset: ReferralCodeCharsetEnum.NUMBERS,
        length: 8,
      });

      if (listOfAvalibaleReferralCodes.includes(pureReferralCode)) {
        generateCode();
      }
      return pureReferralCode;
    };
    return generateCode();
  }
  async create(userId: number) {
    const user = await this.userService.findUniqueUserByUserId(userId);
    const activeReferralCodes =
      await this.referralCodeRepository.findManyUnUsedUserReferralCodes(userId);
    if (activeReferralCodes.length > 10) {
      throw new BadRequestException(
        'کد معرف های استفاده نشده بیشتر از 10 عدد میباشد لطفا از کد معرف های قبلی استفاده نمایید.',
      );
    }
    const userRoleId: number = user['role_id'];
    const referralCodeStr: string = await this.uniqueReferralCode();
    const addReferallCode = await this.referralCodeRepository.create({
      code: referralCodeStr,
      user_id: userId,
      role_id: userRoleId,
      used: false,
    });
    return addReferallCode;
  }

  pureReferralCode(options: randomstring.GenerateOptions) {
    return randomstring.generate(options);
  }
  async findAll(page: number, limit: number, userId: number): Promise<object> {
    const referralCodesCount =
      await this.referralCodeRepository.countUserReferralCode(userId);
    const referralCodes =
      await this.referralCodeRepository.findManyUserReferraCodesByPaginationAndUsedUserRelation(
        {
          skip: paginationSkip(page, limit),
          take: limit,
        },
        userId,
      );
    return {
      page,
      limit,
      total: referralCodesCount,
      list: referralCodes,
    };
  }

  findOne(id: number) {
    return `This action returns a #${id} referralCode`;
  }

  async checkReferralCodeForSignUp(code: string) {
    const referralCode = await this.referralCodeRepository.findOne({
      where: { code },
    });
    return referralCode;
  }

  async useReferralCodeForSignUp(referral_code_id: number, user_id: number) {
    const useReferralCode = await this.referralCodeRepository.update({
      where: { id: referral_code_id },
      data: { used: true, used_by_user_id: user_id, used_at: new Date() },
    });
    return useReferralCode;
  }

  update(id: number, updateReferralCodeDto: UpdateReferralCodeDto) {
    return `This action updates a #${id} referralCode`;
  }

  remove(id: number) {
    return `This action removes a #${id} referralCode`;
  }
}
