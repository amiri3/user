import { Controller, Get, Post, Req, Patch, Request, Param, Delete, HttpCode, HttpStatus, UseInterceptors, Query } from '@nestjs/common';
import { ReferralCodeService } from './referral-code.service';
import { PaginationQueryStringDto } from "../../dto/pagination.dto";
import { AccessTokenInterface } from 'src/interface/access-token.interface';
import { User } from 'src/custom-decorators/user.decorators';

@Controller('/referral-code')
export class ReferralCodeController {
  constructor(private readonly referralCodeService: ReferralCodeService) { }

  @Post('/')
  @HttpCode(HttpStatus.OK)
  create(@Request() request , @User() user : AccessTokenInterface) {
    const userId = user.user_id;
    return this.referralCodeService.create(userId);
  }

  @Get('/')
  @HttpCode(HttpStatus.OK)
  findAll(@Query() { limit, page }: PaginationQueryStringDto, @Req() request) {
    const userId = request?.user?.user_id;
    return this.referralCodeService.findAll(+page, +limit, userId);
  }

  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') id: string) {
    return this.referralCodeService.findOne(+id);
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  remove(@Param('id') id: string) {
    return this.referralCodeService.remove(+id);
  }
}
