import { Injectable, Logger } from "@nestjs/common";
import { Referral_Code } from "@prisma/client";
import { PrismaService } from "src/prisma/prisma.service";




@Injectable()
export class ReferralCodeRepository {
   private readonly logger = new Logger(ReferralCodeRepository.name)
   
   constructor(
      private readonly prismaService : PrismaService
   ){

   }

   async findOneById(id: number): Promise<object> {
      try {
         const result = await this.prismaService.referral_Code.findFirst( { where: { id } });
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }

   async updateByIdAndEmailVerified(status: boolean, user_id: number): Promise<object> {
      try {
         const result = await this.prismaService.referral_Code.update( { where: { id: user_id }, data: {used :  status } });
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }
   async findOne(query: object): Promise<object> {
      try {
         const result = await this.prismaService.referral_Code.findFirst( query);
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }
   async findMany(query: object): Promise<any> {
      try {
         const result = await this.prismaService.referral_Code.findMany( query);
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }
   async findManyUserReferraCodesByPaginationAndUsedUserRelation(pagination : object,user_id : number) : Promise<Array<Referral_Code>> {
      try {
         const result = await this.prismaService.referral_Code.findMany({
            where : {
               user_id
            },
            include: {
               used_by: {
                 select: {
                   phone_number: true,
                   last_name: true,
                   first_name: true,
                 },
               },
             },
            ...pagination
         });
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
      
   } 
   async findManyUnUsedUserReferralCodes(user_id : number): Promise<Array<Referral_Code>> {
      try {
         const result = await this.prismaService.referral_Code.findMany({
            where : {
               used : false,
               user_id 
            }
         });
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }
   async listOfAvalibaleReferralCodes(): Promise<Array<object>> {
      try {
         const result = await this.prismaService.referral_Code.findMany({
            where  : {
            },
            select : {
               code : true
            }
         });
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }
   async count(query: object): Promise<number> {
      try {
         const result = await this.prismaService.referral_Code.count( query);
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }
   async countUserReferralCode(user_id : number): Promise<number> {
      try {
         const result = await this.prismaService.referral_Code.count({
            where : {
               user_id
            }
         });
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }
   async create(data): Promise<any> {
      try {
         const result = await this.prismaService.referral_Code.create( { data })
         return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }

   async update(query: object) {
      try {
         // const result = await this.prismaService.referral_Code.update({
         //    // where : {},
         //    // data : {}
         // });
         return
         // return result
      } catch (error) {
         this.logger.error(error);
         return null
      }
   }
}