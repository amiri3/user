import { Module } from '@nestjs/common';
import { ReferralCodeService } from './referral-code.service';
import { ReferralCodeController } from './referral-code.controller';

import { ReferralCodeRepository } from "./referral-code.repository"
import { UserModule } from "../user/user.module";
import { PrismaModule } from 'src/prisma/prisma.module';


@Module({
  imports: [UserModule , PrismaModule],
  controllers: [ReferralCodeController],
  providers: [ReferralCodeService, ReferralCodeRepository],
  exports: [ReferralCodeService]
})
export class ReferralCodeModule { }
