import { Injectable, NestInterceptor, ExecutionContext, CallHandler, HttpStatus } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
   statusCode: number,
   timestamp: number,
   message: string | null
   data: T;
}

@Injectable()
export class TransformInterceptor<T> implements NestInterceptor<T, Response<T>> {
   intercept(context: ExecutionContext, next: CallHandler): Observable<Response<T>> {
      return next.handle().pipe(map(data => ({ statusCode: HttpStatus.OK, page: data?.page ? data.page : null, limit: data?.limit ? data.limit : null, total: data?.total ? data.total : null, data: data?.list ? data.list : data, message: data?.message || null, timestamp: (new Date()).getTime() })));
   }
}