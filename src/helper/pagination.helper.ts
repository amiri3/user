
export const paginationSkip = (page: number, limit: number): number => page == 1 ? 0 : (page * limit) - 1 ; 
// export function isEmptyObject(obj) {
//     return Object.entries(obj).length === 0;
// }
// export function checkDateRange(current_date:Date, start_date:Date, end_date:Date) {
//   var startDate = new Date(start_date);
//   var endDate = new Date(end_date);
//   var inputDate = new Date(current_date);
//   if(inputDate >= startDate && inputDate <= endDate) {return true} else return false
// }