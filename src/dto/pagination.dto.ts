import { IsNotEmpty, IsNumberString, IsOptional } from 'class-validator';

export class PaginationQueryStringDto {
  @IsNotEmpty()
  page: number;

  @IsNotEmpty()
  limit: number;
}
