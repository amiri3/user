export enum ReferralCodeCharsetEnum {
  NUMBERS = "numeric",
  ALPHABETIC = "alphabetic",
}