export enum Roles {
  USER = 'USER', 
  ADMIN = "ADMIN",
  SUPERADMIN = "SUPERADMIN"
}
export enum RolesActions {
  CREATE_TEAM = 'CREATE_TEAM',
  READ_TEAM = 'READ_TEAM',
  UPDATE_TEAM = 'UPDATE_TEAM',
}
