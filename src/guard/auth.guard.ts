import { CanActivate, Injectable, UnauthorizedException, ExecutionContext, } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { JwtService } from "@nestjs/jwt";
import { jwtConstants } from "src/constants/jwt-constants";
import { IS_PUBLIC_KEY } from "src/custom-decorators/public.decorators";

@Injectable()
export class AuthGuard implements CanActivate {
   constructor(private jwtService: JwtService, private reflector: Reflector) { }

   async canActivate(context: ExecutionContext): Promise<boolean> {
      const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
         context.getHandler(),
         context.getClass(),
      ]);
      if (isPublic) {
         return true;
      }
      const request = context.switchToHttp().getRequest();
      const token = this.extractTokenFromHeader(request);
      if (!token) {
         throw new UnauthorizedException("توکن یافت نشد.");
      }
      let payload;
      try {
         payload = await this.jwtService.verifyAsync(token, {
            secret: jwtConstants.secret,
         });
      } catch (error) {
         throw new UnauthorizedException("توکن ارسال شده منقضی می باشد.");
      }
    
      request['user'] = payload;
      request['authorization'] = token;
      return true;
   }

   private extractTokenFromHeader(request: Request): string | undefined {
      const [type, token] = request.headers?.["authorization"]?.split(' ') ?? [];
      return type === 'Bearer' ? token : undefined;
   }
}
