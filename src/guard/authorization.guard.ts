import { CanActivate, Injectable, UnauthorizedException, ExecutionContext, ForbiddenException, } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { ACTION_KEY } from "../custom-decorators/action.decorators";
import { ROLE_KEY } from "../custom-decorators/role.decorators";
import { RoleService } from "src/module/role/role.service";
import { Roles } from "src/enum/role.enum";
import { AccessTokenInterface } from "src/interface/access-token.interface";

@Injectable()
export class AuthorizationGuard implements CanActivate {
   constructor(private reflector: Reflector, 
      private readonly roleService: RoleService
      ) { }
   async canActivate(context: ExecutionContext): Promise<boolean> {
      const actions: string[] = this.reflector.getAllAndOverride<string[]>(ACTION_KEY, [
         context.getHandler(),
         context.getClass(),
      ])
      const setRoles: string = this.reflector.getAllAndOverride<string>(ROLE_KEY, [
         context.getHandler(),
         context.getClass(),
      ])

      if ((!setRoles || !setRoles.length) && (!actions?.length || !actions)) {
         return true
      }
      const request = context.switchToHttp().getRequest();
      const user : AccessTokenInterface = request?.user;
      const roleId: number = Number(user.role_id);
      const role = await this.roleService.findOneRoleById(roleId);
      if (role?.["name"] == Roles.SUPERADMIN) {
         return true
      }
      if (!user.is_active) {
         throw new ForbiddenException("حساب کاربری شما مسدود می باشد.")
      }
      if (role && setRoles?.includes(role?.['name']) && (!actions?.length || !actions)) {
         return true
      }
      const listActionsByRoleId = await this.roleService.roleAction(roleId);
      const matchRoles = []
      for (let index = 0; index < listActionsByRoleId.length; index++) {
         const roleAction = listActionsByRoleId[index];
         if (actions?.includes(roleAction['method'])) {
            matchRoles.push(roleAction['method'])
         }
      }
      if (matchRoles.length === 0) {
         throw new ForbiddenException("شما دسترسی مورد نیاز به این عملیات را ندارید.");
      }
      return true
   }

}
